module.exports = knex => {
  const express = require('express');

  // AJV
  const { Validator, ValidationError } = require('express-json-validator-middleware');
  const validator = new Validator({
    removeAdditional: true,
    coerceTypes: true,
  });
  require('ajv-keywords')(validator.ajv);
  require('ajv-formats')(validator.ajv);
  const validate = validator.validate;

  validator.ajv.addKeyword({
    keyword: 'isNotEmpty',
    validate: (schema, data) => {
      if (schema) {
        return typeof data === 'string' && data.trim() !== '';
      } else return true;
    },
  });

  const middlewares = require('./middlewares')(ValidationError);
  const api = require('./api')(express, knex, validate);
  const project = require('./constants/project');

  const app = express();

  app.use(express.json());

  app.get('/', (req, res) => {
    res.json({
      message: project.message,
    });
  });

  app.use('/api/v1', api);

  app.use(middlewares.notFound);
  app.use(middlewares.errorHandler);

  return app;
};
