module.exports = ValidationError => {
  const errorTypes = {
    ValidationError: 422,
    UniqueViolationError: 409,
  };

  const errorMessages = {
    UniqueViolationError: 'Already exists.',
  };

  function notFound(req, res, next) {
    const error = new Error(`Not found - ${req.originalUrl}`);
    res.status(404);
    next(error);
  }

  function errorHandler(error, req, res, next) {
    if (error instanceof ValidationError) {
      res.status(400).send(error.validationErrors);
    } else {
      const statusCode = res.statusCode === 200 ? errorTypes[error.name] || 500 : res.statusCode;

      const errorObject = {
        status: statusCode,
        message: errorMessages[error.name] || error.message,
        stack: process.env.NODE_ENV === 'production' ? '🥞' : error.stack,
        errors: error.errors || undefined,
      };

      console.log(`😡 Error: ${JSON.stringify(errorObject, null, 2)}`);

      res.status(statusCode);
      res.json(errorObject);
    }
  }

  return {
    notFound,
    errorHandler,
  };
};
