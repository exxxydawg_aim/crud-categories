module.exports = (express, knex, validate) => {
  const project = require('../constants/project');

  const categories = require('./categories')(express, knex, validate);

  const router = express.Router();

  router.get('/', (req, res) => {
    res.json({
      message: project.message,
    });
  });

  router.use('/categories', categories);

  return router;
};
