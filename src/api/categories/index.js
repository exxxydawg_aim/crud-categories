module.exports = (express, knex, validate) => {
  const router = express.Router();
  const modelFields = ['id', 'slug', 'name', 'description', 'createdDate', 'active'];

  // GET ALL
  router.get(
    '/',
    validate({
      query: {
        type: 'object',
        properties: {
          // Пагинация
          page: {
            type: 'integer',
          },
          pageSize: {
            type: 'integer',
            minimum: 1,
            maximum: 9,
          },
          // Фильтры
          name: {
            type: 'string',
            transform: ['trim'],
          },
          description: {
            type: 'string',
            transform: ['trim'],
          },
          active: {
            type: 'string',
            transform: ['trim'],
            enum: ['0', '1', 'false', 'true'],
          },
          search: {
            type: 'string',
            transform: ['trim'],
          },
          // Сортировка
          sort: {
            type: 'string',
            transform: ['trim', 'toLowerCase'],
          },
        },
      },
    }),
    async (req, res, next) => {
      try {
        // Пагинация
        let { page = 1, pageSize = 2 } = req.query;
        if (page === 0) page = 1;
        // Фильтры
        let { name = null, description = null, active = null, search = null } = req.query;

        if (name) name = name.replace('е', '[е|ё]');
        if (description) description = description.replace('е', '[е|ё]');
        if (search) search = search.replace('е', '[е|ё]');

        // Сортировка
        let { sort = '-createdDate' } = req.query;
        // Если передано значение, отличное от названий полей модели, установим дефолтное значение
        if (sort !== '-createdDate' && !modelFields.includes(sort.slice(1)) && !modelFields.includes(sort)) {
          sort = '-createdDate';
        }
        const sortObj = {
          column: sort.startsWith('-') ? sort.slice(1) : sort,
          order: sort.startsWith('-') ? 'desc' : 'asc',
        };

        const categories = await knex('categories')
          .where(data => {
            if (search) {
              name = null;
              description = null;
              return data.where('name', '~*', search).orWhere('description', '~*', search);
            }
            return data;
          })
          .andWhere(data => (name ? data.where('name', '~*', name) : data))
          .andWhere(data => (description ? data.where('description', '~*', description) : data))
          .andWhere(data => (active ? data.where({ active: !!JSON.parse(active) }) : data))
          .orderBy([sortObj])
          .limit(pageSize)
          .offset(page * pageSize - pageSize);

        return res.status(200).json({
          message: 'Success!',
          pageItems: +categories.length,
          data: categories,
        });
      } catch (e) {
        next(e);
      }
    }
  );

  // GET ONE by slug
  router.get(
    '/slug',
    validate({
      body: {
        type: 'object',
        required: ['slug'],
        properties: {
          slug: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-z0-9\\s]+$/i',
          },
        },
        additionalProperties: false,
      },
    }),
    async (req, res, next) => {
      try {
        const { slug } = req.body;

        const category = await knex('categories').where({ slug: slug }).first();

        if (!category) {
          const e = new Error('Not found');
          res.statusCode = 404;
          return next(e);
        }

        return res.status(200).json({
          message: 'Success!',
          data: category,
        });
      } catch (e) {
        next(e);
      }
    }
  );

  // GET ONE by id
  router.get(
    '/:id',
    validate({
      params: {
        type: 'object',
        required: ['id'],
        properties: {
          id: {
            type: 'string',
            format: 'uuid',
          },
        },
      },
    }),
    async (req, res, next) => {
      try {
        const { id } = req.params;
        const category = await knex('categories').where({ id: id }).first();

        if (!category) {
          const e = new Error('Not found');
          res.statusCode = 404;
          return next(e);
        }

        return res.status(200).json({
          message: 'Success!',
          data: category,
        });
      } catch (e) {
        next(e);
      }
    }
  );

  // CREATE ONE
  router.post(
    '/',
    validate({
      body: {
        type: 'object',
        required: ['slug', 'name', 'active'],
        properties: {
          slug: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-z0-9\\s]+$/i',
          },
          name: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-zа-яё0-9\\s]+$/i',
          },
          description: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-zа-яё0-9\\s]+$/i',
          },
          active: {
            type: 'boolean',
          },
        },
        additionalProperties: false,
      },
    }),
    async (req, res, next) => {
      try {
        const set = req.body;

        const category = await knex('categories').insert(set, ['*']);

        return res.status(201).json({
          message: 'Success!',
          data: category,
        });
      } catch (e) {
        next(e);
      }
    }
  );

  // UPDATE ONE
  router.put(
    '/:id',
    validate({
      params: {
        type: 'object',
        required: ['id'],
        properties: {
          id: {
            type: 'string',
            format: 'uuid',
          },
        },
      },
      body: {
        type: 'object',
        properties: {
          slug: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-z0-9\\s]+$/i',
          },
          name: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-zа-яё0-9\\s]+$/i',
          },
          description: {
            type: 'string',
            isNotEmpty: true,
            transform: ['trim'],
            regexp: '/^[a-zа-яё0-9\\s]+$/i',
          },
          active: {
            type: 'boolean',
          },
        },
        additionalProperties: false,
      },
    }),
    async (req, res, next) => {
      try {
        const { id } = req.params;
        // remove id and createdDate from body
        const set = req.body;

        const category = await knex('categories').where({ id: id }).first();

        if (!category) {
          const e = new Error('Not found');
          res.statusCode = 404;
          return next(e);
        }

        const updatedCategory = await knex('categories').where({ id: id }).update(set, ['*']);

        return res.status(200).json({
          message: 'Success!',
          data: updatedCategory,
        });
      } catch (e) {
        next(e);
      }
    }
  );

  // REMOVE ONE
  router.delete(
    '/:id',
    validate({
      params: {
        type: 'object',
        required: ['id'],
        properties: {
          id: {
            type: 'string',
            format: 'uuid',
          },
        },
      },
    }),
    async (req, res, next) => {
      try {
        const { id } = req.params;

        await knex('categories').where({ id: id }).del();

        return res.status(200).json({
          message: 'Success!',
        });
      } catch (e) {
        next(e);
      }
    }
  );

  return router;
};
