const knex = require('./db');
const app = require('./app')(knex);
const logger = require('./lib/logger');

const port = process.env.SERVER_PORT || 9000;
const host = process.env.SERVER_IP || '127.0.0.1';

(async () => {
  try {
    await knex.migrate.latest();
    await knex.seed.run();
  } catch (e) {
    logger.info(e);
    process.exit();
  }
})();

app.listen(port, host, () => {
  logger.info(`Listening at http://${host}:${port}`);
});
