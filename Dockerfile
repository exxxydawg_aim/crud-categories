FROM node:16.14

WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm i

COPY . .
EXPOSE 9000
CMD ["npm", "start"]


