# CRUD Categories project

Для справки: при каждом запуске сервера БД очищается и заполняется тестовыми данными.

## Start

```sh
docker-compose build
docker-compose up -d
```

## Postman

В директории `postman_collection` хранятся файлы с расширением `.json` с коллекцией и переменными для импорта.
