const tableName = 'categories';

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex(tableName).del();
  await knex(tableName).insert([
    { slug: 'slug1', name: 'Мёд из пчел', description: 'Описание категории Мёд из пчел', active: true },
    { slug: 'slug2', name: 'Мёд не из пчел', description: 'Описание категории Мёд не из пчел', active: true },
    { slug: 'slug3', name: 'Не Мёд', description: '', active: false },
  ]);
};
